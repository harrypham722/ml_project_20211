# Import dung cho PyApp (UI)
import cv2
import tkinter
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename
import PIL.Image, PIL.ImageTk
from tkscrolledframe import ScrolledFrame
# Import dung cho test model
import os, sys
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from keras.models import load_model

# Lấy model đã traning ra để test
model = load_model('./training/TSR.h5')

#Định nghĩa các lớp của biển báo giao thông
classes = { 0:'Gioi han toc do (20km/h)',
			1:'Gioi han toc do (30km/h)',
			2:'Gioi han toc do (50km/h)',
			3:'Gioi han toc do (60km/h)',
			4:'Gioi han toc do (70km/h)',
			5:'Gioi han toc do (80km/h)',
			6:'Ket thuc gioi han toc do (80km/h)',
			7:'Gioi han toc do (100km/h)',
			8:'Gioi han toc do (120km/h)',
			9:'Cam vuot',
			10:'Cam o to tai vuot',
			11:'Duong giao nhau',
			12:'Duong uu tien',
			13:'Giao nhau voi duong uu tien',
			14:'Dung lai!',
			15:'Duong cam',
			16:'Cam xe tai',
			17:'Duong mot chieu',
			18:'Chu y nguy hiem',
			19:'Chu y doan duong cong re trai nguy hiem',
			20:'Chu y doan duong cong re phai nguy hiem',
			21:'Chu y doan duong cong kep',
			22:'Canh bao duong gap ghenh',
			23:'Canh bao duong tron truot',
			24:'Duong thu hep tu ben phai',
			25:'Doan duong dang thi cong',
			26:'Canh bao co den giao thong',
			27:'Canh bao duong nguoi di bo cat ngang',
			28:'Canh bao doan duong hay co tre em',
			29:'Canh bao doan duong danh cho xe dap',
			30:'Canh bao nguy co tron truot do tuyet',
			31:'Canh bao doan duong hay co dong vat hoang da',
			32:'Bo het tat ca lenh cam',
			33:'Chi duoc re phai o phia truoc',
			34:'Chi duoc re trai o phia truoc',
			35:'Chi duoc di thang',
			36:'Di thang hoac re phai',
			37:'Di thang hoac re trai',
			38:'Tiep tuc di sang ben phai',
			39:'Tiep tuc di sang ben trai',
			40:'Noi giao nhau chay theo vong xuyen',
			41:'Het cam vuot',
			42:'Het cam o to tai vuot'
}

# Hàm dùng để test một ảnh cụ thể
def test_on_particular_image(img_path):
	data = []
	image = Image.open(img_path)
	image = image.resize((30,30))
	data.append(np.array(image))
	X_test = np.array(data)
	pred = np.argmax(model.predict(X_test), axis=1)
	return image, pred

# Tao cua so App
rootApp = Tk()
rootApp.title('HKTL Traffic Sign Scan App')
w0, h0 = rootApp.winfo_screenwidth(), rootApp.winfo_screenheight()
#rootApp.overrideredirect(1)		#Full man hinh
rootApp.geometry("%dx%d+0+0" %(w0*0.8, h0*0.92))
#rootApp.configure(bg = '#FF66FF')

# Label Header
hlabel = Label(rootApp, text = '	Traffic Sign Scanning and Classification Application	', fg = '#008000', bg = '#FF99FF', font = ('Times New Roman' , 20, 'bold'))
hlabel.pack(side = 'top')
frameSpace = Frame(rootApp, width = 10, height = 10)
frameSpace.pack(side = 'top')

# # Tao Frame de de quan ly cac object
# frame0 = Frame(rootApp, width = w0*0.85, height = h0*0.9)
# frame0.pack(side="top", expand=1, fill="both")
# # Tao scroll cho Frame
# sf = ScrolledFrame(frame0, width = w0*0.85, height = h0*0.9)
# sf.pack(side="top", expand=1, fill="both")
# # Bind the arrow keys and scroll wheel
# sf.bind_arrow_keys(frame0)
# sf.bind_scroll_wheel(frame0)
# frame = sf.display_widget(Frame)

# Tao label hien thi thong bao huong dan
label0 = Label(rootApp, text='Welcome to our App! \n Please choose a file or use your camera for scanning traffic sign!', fg = 'black', font = ("Times New Roman", 14, 'bold'))
#label0.grid(column = 0, row = 2)
#label0.pack()
label0.place(x = 5, y = 45)

canvasImg = Canvas(rootApp, bg = '#97FFFF', width = 600 , height = 500)
canvasImg.place(x = 10, y = 130)
def eventClick0():	# Thuc hien khi bam nut "Import File"
	#labelError.destroy()
	labelError.configure(text = '')
	global img, canvasImg, photo, filename
	label0.configure(text = "Image is selected! Please click /Scan/ to get message from traffic sign in your photo!")
	filename = askopenfilename()	#Yeu cau select file va tra ve path
	#print(filename)
	img = cv2.imread(filename)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	#cv2.imshow('Display', img)
	(h1, w1, d1) = img.shape
	#if h1 > h0//2:
		#img = cv2.resize(img, dsize = None, fx = 0.5, fy = 0.5)
	img = cv2.resize(img, (600, 500))
	#canvasImg.configure()
	#else:
	# 	canvasImg.configure(width = w1 , height = h1)
	#canvasImg.grid(column = 0, row = 4)
	photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(img))
	canvasImg.create_image(0, 0, image = photo, anchor = NW)
	return

labelError = Label(rootApp, fg = 'red', font = (12))
def eventClick1():	# Thuc hien khi bam nut "Turnon Camera"
	#labelError.grid(column = 0, row = 4)
	labelError.place(x = 120, y = 130)
	labelError.configure(text = 'Sorry! Feature not implimented yet!')
	return

# Them button
button0 = Button(rootApp, text = "Import File", command = eventClick0, bg = 'black', fg = 'white')
#button0.grid(column = 0, row = 3)
button1 = Button(rootApp, text = "Turn on Camera", command = eventClick1, bg = 'black', fg = 'white')
#button1.grid(column = 1, row = 3)
button0.place(x = 150, y = 95)
button1.place(x = 250, y = 95)

labelMs = Label(rootApp, text = 'MESSAGE: ---', fg = 'black', font = ("Times New Roman", 22, 'bold'))
#labelMs.grid(column = 0, row = 6)
labelMs.place(x = 150, y = 670)

def eventScan():
	#sys.argv[1] IndexError: list index out of range
	image, pred = test_on_particular_image(filename)
	s = [str(i) for i in pred]
	a = int("".join(s))
	message0 = 'MESSAGE:  ' + str(classes[a])
	# labelMs = Label(frame, text = message0, fg = 'black', font = ("Times New Roman", 16, 'bold'))
	# labelMs.grid(column = 0, row = 5)
	labelMs.configure(text = message0)

buttonScan = Button(rootApp, text = "Scan to get mesage", command = eventScan, bg = 'black', fg = 'white', font = ('bold'))
#buttonScan.grid(column = 0, row = 5)
buttonScan.place(x = 180, y = 635)

# Chon DarkMode cho UI
cDarkMode = BooleanVar()
#lblSpace = Label(frame, text = '				')
checkDarkMode = ttk.Checkbutton(rootApp, text='Dark Mode', var = cDarkMode)
checkDarkMode.place(x = 850, y = 125)
def runDarkMode():
	if checkDarkMode.instate(['selected']):
		rootApp.configure(bg = 'black')
		#frame.configure(bg = 'black')
		frameSpace.configure(bg = 'black')
		#lblSpace.configure(bg = 'black')
		label0.configure(bg = 'black', fg = 'white')
		labelError.configure(bg = 'black', fg = '#CCFF66')
		labelMs.configure(bg = 'black', fg = 'white')
		labelSetting.configure(bg = 'black', fg = 'white')
		canvasImg.configure(bg = '#330000')
		#sf.configure(bg = 'black')
		hlabel.configure(bg = '#008000', fg = '#FF99FF')
		#checkDarkMode.configure(bg = 'black', fg = 'white')
		button0.configure(bg = 'white', fg = 'black')
		button1.configure(bg = 'white', fg = 'black')
		buttonScan.configure(bg = 'white', fg = 'black')
		#checkDarkMode.configure(activebackground = 'black', activebackground = 'white')
	else:
		rootApp.configure(bg = 'white')
		frameSpace.configure(bg = 'white')
		label0.configure(bg = 'white', fg = 'black')
		labelError.configure(bg = 'white', fg = 'red')
		labelMs.configure(bg = 'white', fg = 'black')
		labelSetting.configure(bg = 'white', fg = 'black')
		canvasImg.configure(bg = '#97FFFF')
		hlabel.configure(bg = '#FF99FF', fg = '#008000')
		button0.configure(bg = 'black', fg = 'white')
		button1.configure(bg = 'black', fg = 'white')
		buttonScan.configure(bg = 'black', fg = 'white')
		#checkDarkMode.configure(activebackground = 'white', activebackground = 'black')
#lblSpace.grid(column = 3)
labelSetting = Label(rootApp, text='Settings:', fg = 'black', font = ("Times New Roman", 14, 'bold'), anchor = 'ne')
#labelSetting.grid(column = 4, row = 2)
checkDarkMode.configure(command = runDarkMode)
#checkDarkMode.grid(column = 4, row = 3)
labelSetting.place(x = 850, y = 100)

# Chay va giu giao dien App
rootApp.mainloop(0)