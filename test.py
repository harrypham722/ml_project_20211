import os, sys
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from keras.models import load_model
cur_path = os.getcwd()
# model = load_model(os.path.join('./training/TSR.h5'))
# Lấy model đã traning ra để test
model = load_model('../training/TSR.h5')

#Định nghĩa các lớp
classes = { 0:'Gioi han toc do (20km/h)',
            1:'Gioi han toc do (30km/h)',
            2:'Gioi han toc do (50km/h)',
            3:'Gioi han toc do (60km/h)',
            4:'Gioi han toc do (70km/h)',
            5:'Gioi han toc do (80km/h)',
            6:'Ket thuc gioi han toc do (80km/h)',
            7:'Gioi han toc do (100km/h)',
            8:'Gioi han toc do (120km/h)',
            9:'Cam vuot',
            10:'Cam o to tai vuot',
            11:'Duong giao nhau',
            12:'Duong uu tien',
            13:'Giao nhau voi duong uu tien',
            14:'Dung lai!',
            15:'Duong cam',
            16:'Cam xe tai',
            17:'Duong mot chieu',
            18:'Chu y nguy hiem',
            19:'Chu y doan duong cong re trai nguy hiem',
            20:'Chu y doan duong cong re phai nguy hiem',
            21:'Chu y doan duong cong kep',
            22:'Canh bao duong gap ghenh',
            23:'Canh bao duong tron truot',
            24:'Duong thu hep tu ben phai',
            25:'Doan duong dang thi cong',
            26:'Canh bao co den giao thong',
            27:'Canh bao duong nguoi di bo cat ngang',
            28:'Canh bao doan duong hay co tre em',
            29:'Canh bao doan duong danh cho xe dap',
            30:'Canh bao nguy co tron truot do tuyet',
            31:'Canh bao doan duong hay co dong vat hoang da',
            32:'Bo het tat ca lenh cam',
            33:'Chi duoc re phai o phia truoc',
            34:'Chi duoc re trai o phia truoc',
            35:'Chi duoc di thang',
            36:'Di thang hoac re phai',
            37:'Di thang hoac re trai',
            38:'Tiep tuc di sang ben phai',
            39:'Tiep tuc di sang ben trai',
            40:'Noi giao nhau chay theo vong xuyen',
            41:'Het cam vuot',
            42:'Het cam o to tai vuot'
}

# Hàm dùng để test một ảnh cụ thể
def test_on_particular_image(img):
    data = []
    image = Image.open(img)
    image = image.resize((30,30))
    data.append(np.array(image))
    X_test = np.array(data)
    pred = np.argmax(model.predict(X_test), axis=1)
    return image, pred

# Khi chạy app nhận diện sẽ gửi vào đường dẫn của một ảnh, ta cần đọc ra 
# để test ảnh đó
image_path = sys.argv[1]
image_path = os.path.join(cur_path, image_path)
image, pred = test_on_particular_image(image_path)
s = [str(i) for i in pred]
a = int("".join(s))

# Trả về kết quả cho app
sys.stdout.write(classes[a])